
Forward Notify

Extention for forward module.
The module provides notification to configurable email address about forward operation on the site.
Uses notification_emails module

To install, place the entire module folder into your modules directory.
Go to Administer -> Site Building -> Modules and enable the Forward Notify module.

To change settigns for notification email go to 
Administer -> Site Configuration -> Notification Emails -> Forward Notify

